import React from "react";

import './spinner.css';

const Spinner = () => {
    return (
        <div className="loadingio-spinner-gear-ggo4ke45pp6">
            <div className="ldio-ngexsta8e2j">
                <div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
    )
};

export default Spinner;