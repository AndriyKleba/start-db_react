import React, {Component} from "react";
import ItemList from "../item-list";
import PersonDetails from "../person-details";
import ErrorIndicator from "../error-indicator";
import SwapicoService from "../../services/swapico-services";
import Row from "../row";

import './people-page.css'

export default class PeoplePage extends Component {

    swapicoService = new SwapicoService()

    state = {
        selectedPerson: 3,
        hasError: false
    }

    componentDidCatch(error, errorInfo) {
        this.setState({hasError: true})
    }

    onPersonSelected = (id) => {
        this.setState({
            selectedPerson: id
        });
    };

    render() {
        if (this.state.hasError) {
            return <ErrorIndicator/>
        }
        const itemList = (<ItemList
            onItemSelected={this.onPersonSelected}
            getDate={this.swapicoService.getAllPeople}
        >
            {item => `${item.name} - (${item.gender}, ${item.birthYear || 'dont have birthday date'})`}
        </ItemList>)

        const personDetails = (<PersonDetails personId={this.state.selectedPerson}/>)
        return (
            <Row leftElement={itemList} rightElement={personDetails}/>
        )
    }
}


